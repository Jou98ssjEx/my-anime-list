import { FC } from "react"
import { ICustomAnnime } from '../../interface';

export const AnimeContent:FC<ICustomAnnime> = ({
  id,
  img,
  name
}) => {

  console.log(name);
  return (
    <div className="card">
      <img src={img} alt={name} />

      <span>
        {name}
      </span>
    </div>
  )
}
