import { AnimeContent } from "./AnimeContent"
import { GroupAnime } from "./GroupAnime"
import { useState, useEffect, ChangeEvent, KeyboardEvent } from 'react';
import { ICustomAnnime } from "../../interface";

export const MainContent = () => {

  const [onSelectAnime, setOnSelectAnime] = useState<ICustomAnnime>();

  const [textAnime, setTextAnime] = useState<string>('');

  const handleChange = ( e: ChangeEvent<HTMLInputElement>) =>{

    setTextAnime( e.target.value);

  }

  const sendText = ( e: KeyboardEvent<HTMLInputElement>) => {
    console.log(e);
  }

  console.log(textAnime);
  

  return (
    <main className="container mt-4">

      <div className="row">
        <div className="col-4">
          <input 
            type="text" 
            className="form-control"
            onChange={ handleChange }
            onKeyUp={ sendText  }
          />
        </div>
      </div>

      <div className="row mt-4">
        <div className="col-8">
          <GroupAnime 
            setOnSelectAnime={setOnSelectAnime} 
            textAnime={textAnime}
          />
        </div>
        <div className="col-4">
          {
            onSelectAnime ? (
              <AnimeContent {...onSelectAnime } />
            ) : ('Selecione un anime')
          }
        </div>
      </div>
    </ main>
  )
}
