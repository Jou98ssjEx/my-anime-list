import React, { FC } from 'react'
import { ICustomAnnime } from '../../interface/ICustomAnime';


interface Props {
  data: ICustomAnnime,
  setOnSelectAnime: (data:ICustomAnnime) => void,

}

export const SingleAnime:FC<Props> = ({data, setOnSelectAnime}) => {

  const { id, name, img } = data;


  const handleSelectAnime = () => {
    console.log({name, img, id});
    setOnSelectAnime(data)
  }

  return (
    <div className='col-3'>
      {/* SingleAnime */}
      <div 
        className="card"
        onClick={handleSelectAnime}
      >
        <img src={img} alt={name} />
      </div>
    </div>
  )
}
