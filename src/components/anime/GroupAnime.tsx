import { SingleAnime } from './SingleAnime'
import { useFetchApi } from '../../hooks';
import { ICustomAnnime } from '../../interface';
import { FC } from 'react';

interface Props {
  setOnSelectAnime: (data: ICustomAnnime) => void;
  textAnime: string;
}

export const GroupAnime:FC<Props> = ({setOnSelectAnime, textAnime}) => {

  const { count, data  } = useFetchApi(textAnime);
  console.log(data);

  return (
    <div className='card'>
      {/* GroupAnime */}
      <div className="row">

        {
          data && (
            data.map( (d: ICustomAnnime) => (
              <SingleAnime 
                key={d.id}
                data={d}
                setOnSelectAnime={setOnSelectAnime}
              />
            ))
          )
        }
        
        
      </div>
    </div>
  )
}
