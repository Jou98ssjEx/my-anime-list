import { Footer, MainContent, Navbar } from "../components"

export const SearchAnime = () => {
  return (
    <>
      <Navbar />
      <MainContent />
      <Footer />        
    </>
  )
}
