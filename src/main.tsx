import React from 'react'
import ReactDOM from 'react-dom/client'
import { AppAnimeList } from './AppAnimeList'

import './main.css';


ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <AppAnimeList />
  </React.StrictMode>
)
