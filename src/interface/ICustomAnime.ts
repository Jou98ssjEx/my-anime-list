

export interface ICustomAnnime{
    date_end   : string,
    date_start : string,
    id         : string,
    img        : string,
    n_cap      : number,
    name       : string,
    sipnosis   : string,
    status     : string,
    type       : string,
}