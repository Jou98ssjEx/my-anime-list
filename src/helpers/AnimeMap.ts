import { Datum } from "../interface";


export const animeMap = ( data : Datum[] ) =>
    ( (!data) ? [] : data.map( (d) =>({
        id: d.id,
        type: d.type,
        name: d.attributes.canonicalTitle,
        date_start: d.attributes.startDate,
        date_end: d.attributes.endDate,
        n_cap: d.attributes.episodeCount,
        img: d.attributes.posterImage.medium,
        status: d.attributes.status,
        sipnosis: d.attributes.synopsis
    })))