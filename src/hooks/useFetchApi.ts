import { useEffect, useState } from "react"
import { API_KITSU } from "../api";
import { IAnimeAPI } from "../interface";
import { animeMap } from '../helpers/AnimeMap';


interface Props {

    nameAnime: string

}

// export function useFetchApi <T>  (   )  {
export const useFetchApi = ( name?: string, limit?: string  ) => {

    const replaceName = (name) ? `${name.toLowerCase().replaceAll(' ', '-')}` : '';

    const [dataAPi, setDataApi] = useState<IAnimeAPI>();

    const [textName, setTextName] = useState(name);

    useEffect(() => {
      setTextName(name);
    }, [name])
    

    const URL = `${API_KITSU}/anime?filter[text]=${ textName }`

    console.log(dataAPi)

    useEffect(() => {
      fetch(URL)
    //   fetch(`${API_KITSU}/anime`)
            .then(resp => resp.json())
            .then(setDataApi)
    }, [URL])

    const dataMap = animeMap(dataAPi?.data || []);

    // console.log(a)


    return{
        data: dataMap,
        count: 0,
        pagination:{}
    }
    
}